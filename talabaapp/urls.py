from django.urls import path
from .views import yuriqnoma, dars_jadval, reting, iqtidorli_talabalar, eslatma, qabul_kvota, qabul_nizom


urlpatterns = [
	# Talabalar
    path('talabalar/yuriqnoma', yuriqnoma, name = 'yuriqnoma'),
    path('talabalar/dars-jadval', dars_jadval, name = 'dars_jadval'),
    path('talabalar/reting', reting, name = 'reting'),
    path('talabalar/iqtidorli-talabalar', iqtidorli_talabalar, name = 'iqtidorli_talabalar'),
    # Abituriyentlar
    path('abituriyentlar/eslatma', eslatma, name = 'eslatma'),
    path('abituriyentlar/qabul-kvota', qabul_kvota, name = 'qabul_kvota'),
    path('abituriyentlar/qabul-nizom', qabul_nizom, name = 'qabul_nizom'),

]