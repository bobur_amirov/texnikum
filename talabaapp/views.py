from django.shortcuts import render

# Create your views here.
def yuriqnoma(request):
	return render(request, 'talabalar/yuriqnoma.html')

def dars_jadval(request):
	return render(request, 'talabalar/dars_jadval.html')

def reting(request):
	return render(request, 'talabalar/reting.html')

def iqtidorli_talabalar(request):
	return render(request, 'talabalar/iqtidorli_talabalar.html')

def eslatma(request):
	return render(request, 'abituriyentlar/eslatma.html')

def qabul_kvota(request):
	return render(request, 'abituriyentlar/qabul_kvota.html')

def qabul_nizom(request):
	return render(request, 'abituriyentlar/qabul_nizom.html')