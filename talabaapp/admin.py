from django.contrib import admin
from .models import Yunalish, DarsJadvalFayl, DarsJadval, IqtidorliTalaba

# Register your models here.

admin.site.register(Yunalish)
admin.site.register(DarsJadvalFayl)
admin.site.register(DarsJadval)
admin.site.register(IqtidorliTalaba)