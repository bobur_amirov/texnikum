from django.db import models

# Create your models here.

class Yunalish(models.Model):
    nomi = models.CharField(max_length=100)

    def __str__(self):
        return self.nomi


class DarsJadval(models.Model):
    hafta = (
        ('Dushanba', 'Dushanba'),
        ('Seshanba', 'Seshanba'),
        ('Chorshanba', 'Chorshanba'),
        ('Payshanba', 'Payshanba'),
        ('Juma', 'Juma'),
        ('Shanba', 'Shanba'),
    )
    yunalish = models.ForeignKey(Yunalish, on_delete=models.CASCADE)
    guruh = models.IntegerField()
    hafta_kuni = models.CharField(max_length=12, choices=hafta)
    para_1 = models.CharField(max_length=50)
    oqituvchi_1 = models.CharField(max_length=50)
    para_2 = models.CharField(max_length=50)
    oqituvchi_2 = models.CharField(max_length=50)
    para_3 = models.CharField(max_length=50)
    oqituvchi_3 = models.CharField(max_length=50)
    para_4 = models.CharField(max_length=50)
    oqituvchi_4 = models.CharField(max_length=50)
    para_5 = models.CharField(max_length=50)
    oqituvchi_5 = models.CharField(max_length=50)

class DarsJadvalFayl(models.Model):
    yunalish = models.ForeignKey(Yunalish, on_delete=models.CASCADE)
    fayl = models.FileField()

class IqtidorliTalaba(models.Model):
    ism_familiya_sharf = models.CharField(max_length=50)
    rasm = models.ImageField(upload_to='iqtidorlitalabalar/')
    matn = models.TextField()

    def __str__(self):
        return self.ism_familiya_sharf
