from django.apps import AppConfig


class TexnikumappConfig(AppConfig):
    name = 'texnikumapp'
