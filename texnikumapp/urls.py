from django.urls import path
from .views import ( home, texnikum_tarix, rahbariyat, kafedralar, markaz_va_bulimlar, aloqalar, yangiliklar, elonlar, 
                    bitta_elon, bitta_yangilik, qonunlar, farmonlar, qarorlar, nizomlar_va_qoidalar, fotolar, vediolar )

app_name = 'texapp'

urlpatterns = [
    path('', home, name = 'home'),
    # texnikum
    path('texnikum/tarix', texnikum_tarix, name = 'texnikum_tarix'),
    path('texnikum/rahbariyat', rahbariyat, name = 'rahbariyat'),
    path('texnikum/kafedralar', kafedralar, name = 'kafedralar'),
    path('texnikum/markaz-va-bulimlar', markaz_va_bulimlar, name = 'markaz_va_bulimlar'),
    path('texnikum/aloqalar', aloqalar, name = 'aloqalar'),
    # yangiliklar
    path('yangiliklar/yangiliklar', yangiliklar, name = 'yangiliklar'),
    path('yangiliklar/yangilik/<int:pk>', bitta_yangilik, name = 'yangilik'),
    path('yangiliklar/elonlar', elonlar, name = 'elonlar'),
    path('yangiliklar/elon/<int:pk>', bitta_elon, name = 'elon'),
    # hujjatlar
    path('hujjatlar/qonunlar', qonunlar, name = 'qonunlar'),
    path('hujjatlar/farmonlar', farmonlar, name = 'farmonlar'),
    path('hujjatlar/qarorlar', qarorlar, name = 'qarorlar'),
    path('hujjatlar/nizomlar-va-qoidalar', nizomlar_va_qoidalar, name = 'nizomlar_va_qoidalar'),
    # galeriya
    path('galeriya/fotolar', fotolar, name = 'fotolar'),
    path('galeriya/vediolar', vediolar, name = 'vediolar'),

]