from django.db import models

# Create your models here.
class Rahbariyat(models.Model):
	lavozim = models.CharField(max_length=50)
	ism_familiya_sharf = models.CharField(max_length=50)
	rasm = models.ImageField(upload_to = 'rahbarlar/')
	qabul_vaqti = models.CharField(max_length=30)
	telefon = models.IntegerField()
	elektrion_pochta = models.EmailField()
	vazifalar = models.TextField()
	biografiya = models.TextField()

	def __str__(self):
		return self.lavozim + '  ' + self.ism_familiya_sharf


class Kafedra(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class KafedraMalumot(models.Model):
	nomi = models.ForeignKey(Kafedra, on_delete = models.CASCADE)
	mudir_ism_familiya_sharf = models.CharField(max_length=50)
	rasm = models.ImageField(upload_to = 'kafedra/')
	qabul_vaqti = models.CharField(max_length=30)
	telefon = models.IntegerField()
	elektrion_pochta = models.EmailField()
	biografiya = models.TextField()
	kafedra_haqida = models.TextField()

	def __str__(self):
		return self.nomi + '  ' + self.mudir_ism_familiya_sharf

class BulimVaMarkazlar(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class BulimVaMarkazlarMalumot(models.Model):
	nomi = models.ForeignKey(BulimVaMarkazlar, on_delete = models.CASCADE)
	ism_familiya_sharf = models.CharField(max_length=50)
	rasm = models.ImageField(upload_to = 'bulimlar/')
	qabul_vaqti = models.CharField(max_length=30)
	telefon = models.IntegerField()
	elektrion_pochta = models.EmailField()
	biografiya = models.TextField()
	kafedra_haqida = models.TextField()

	def __str__(self):
		return self.ism_familiya_sharf

class Yangiliklar(models.Model):
	sarlavha = models.CharField(max_length=200)
	rasm = models.ImageField(upload_to='yangiliklar/')
	matn = models.TextField()
	fayl_yul = models.URLField(blank=True)
	sana = models.DateField(auto_now_add=True)
	yan_kurilgan = models.IntegerField(default=0)

	def __str__(self):
		return self.sarlavha

class Elonlar(models.Model):
	sarlavha = models.CharField(max_length=200)
	rasm = models.ImageField(upload_to='elonlar/')
	matn = models.TextField()
	fayl_yul = models.URLField(blank=True)
	sana = models.DateField(auto_now_add=True)
	elon_kurilgan = models.IntegerField(default=0)


	def __str__(self):
		return self.sarlavha

class Fotolar(models.Model):
	name = models.CharField(max_length=100)
	rasmlar = models.ImageField(upload_to='fotolar/')
	# sana = models.DateField(auto_now_add=True)