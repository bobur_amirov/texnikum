from django.contrib import admin
from .models import Kafedra, Yangiliklar, Elonlar, Rahbariyat, KafedraMalumot, BulimVaMarkazlar, BulimVaMarkazlarMalumot, Fotolar

# Register your models here.
admin.site.register(Kafedra)
admin.site.register(Yangiliklar)
admin.site.register(Elonlar)
admin.site.register(Rahbariyat)
admin.site.register(KafedraMalumot)
admin.site.register(BulimVaMarkazlar)
admin.site.register(BulimVaMarkazlarMalumot)
admin.site.register(Fotolar)