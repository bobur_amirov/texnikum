from django.shortcuts import render, get_object_or_404
from .models import Yangiliklar, Elonlar, Fotolar
from django.core.paginator import Paginator, EmptyPage

# Create your views here.

def home(request):
	fotolar = Fotolar.objects.order_by('-id')[:6]
	elon1 = Elonlar.objects.order_by('-id')[:1]
	elonlar = Elonlar.objects.order_by('-id')[1:3]
	yangilik1 = Yangiliklar.objects.order_by('-id')[:1]
	yangiliklar = Yangiliklar.objects.order_by('-id')[1:3]

	context = {
		'fotolar':fotolar,
		'elon1':elon1,
		'elonlar':elonlar,
		'yangilik1':yangilik1,
		'yangiliklar':yangiliklar,
	}
	return render(request, 'home.html', context)

def texnikum_tarix(request):
	return render(request, 'texnikum/texnikum_tarix.html')

def rahbariyat(request):
	return render(request, 'texnikum/rahbariyat.html')

def kafedralar(request):
	return render(request, 'texnikum/kafedralar.html')

def markaz_va_bulimlar(request):
	return render(request, 'texnikum/markaz_va_bulimlar.html')

def aloqalar(request):
	return render(request, 'texnikum/aloqalar.html')

def fotolar(request):
	return render(request, 'galeriya/fotolar.html')

def vediolar(request):
	return render(request, 'galeriya/vediolar.html')

def yangiliklar(request):
	yangiliklar = Yangiliklar.objects.all()
	pgn = Paginator(yangiliklar, 2)
	page_nums = request.GET.get('page',1)
	try:
		page = pgn.page(page_nums)
	except EmptyPage:
		page = pgn.page(1)
	context = {
		'yangiliklar':page
	}
	return render(request, 'yangiliklar/yangiliklar.html', context)

def bitta_yangilik(request, pk):
	yangilik = get_object_or_404(Yangiliklar, id=pk)
	yangilik.yan_kurilgan = yangilik.yan_kurilgan + 1
	yangilik.save()
	return render(request, 'yangiliklar/yangilik.html', {'yangilik':yangilik})

def elonlar(request):
	elonlar = Elonlar.objects.all()
	pgn = Paginator(elonlar, 2)
	page_nums = request.GET.get('page', 1)
	try:
		page = pgn.page(page_nums)
	except EmptyPage:
		page = pgn.page(1)
	context = {
		'elonlar':page
	}
	return render(request, 'yangiliklar/elonlar.html', context)

def bitta_elon(request, pk):
	elon = get_object_or_404(Elonlar, id=pk)
	elon.elon_kurilgan = elon.elon_kurilgan + 1
	elon.save()

	return render(request, 'yangiliklar/elon.html', {'elon':elon})
	
def qonunlar(request):
	return render(request, 'hujjatlar/qonunlar.html')

def farmonlar(request):
	return render(request, 'hujjatlar/farmonlar.html')

def qarorlar(request):
	return render(request, 'hujjatlar/qarorlar.html')

def nizomlar_va_qoidalar(request):
	return render(request, 'hujjatlar/nizomlar_va_qoidalar.html')
